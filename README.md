# Ce dépôt contient les fichiers utilisés pour la réalisation du projet Talisman du fablab la Bobine.

## Présentation du projet

L'objectif de ce projet est de modéliser la circulation des information lors du choc d'un véhicule (Renault Talisman) contre un obstacle.
Une tablette sert de commande, les flux d'information sont représentés par des rubans de LEDs.

## Architecture

Le projet se compose 
*  d'un véhicule Talisman "préparé" pour cet usage
*  d'une tablette sous Androïd servant d'interface de commande
*  d'un module informatique embarqué servant à la gestion du Wifi et de commande des rubans de LEDs
*  de ruabns de LEDs

Voici un schéma fonctionnel de l'ensemble

![schema-fonctionnel](./images/schema_fonctionnel.png)

## Matériel utilisé

* 2 cartes ESP32  Wroom nodeMCU pour la liaison wifi avec la tablette
* des bandeaux de LEDS ws2812b
* une tablette Android

## Organisation du code

Un des microcontrôleurs ESP32 (ESP-WebServer) est le chef d'orchestre du système : il est programmé en tant qu'Access-Point afin de créer un réseau wifi auquel seront connectés la tablette et le second ESP32 (ESP32-Ruban).


La tablette, soit à l'aide d'un navigateur internet, soit en utilisant l'application "Talisman" envoie des codes de commande qui sont reçus par le WebServer et transmis après analyse par liaison série à ESP32-Ruban qui pilotera en conséquense les rubans de leds.

Il sera aussi possible de mettre à jour le firmware (logiciel) par liaison wifi (Over The Air) afin de ne pas avoir à utiliser de câble pour mettre à jour le système embarqué.

## Installation
Pensez à renseigner le fichier /WebServer/src/settings.h en complétant les variables avec le nom du réseau (SSID) et le mot de passe du réseau. Idem pour le fichier /Rubans/src/settings.h
Rien de particulier à dire, depuis son IDE favori, on upload le firmware par la connexion série (câble USB). Attention quand même à ne pas se tromper de microcontroleur !!!
Il est possible aussi de faire la mise à jour (sauf la première) en utilisant le wifi : votre PC, tablette, doit être connecté au réseau "talisman", puis on se rend sur la page "192.168.4.1:8888"
et on clique sur le bon bouton, puis on cherche le fichier firmware.bin qui va bien et hop !

Attention bis ! Il ne faut pas oublier de télécharger les fichiers présents dans /data/* dans le système de fichiers SPIFFS. On peut utiliser pour cela l'extension : voir ici pour l'[IDE Arduino](https://techtutorialsx.com/2018/08/24/esp32-arduino-spiffs-file-upload-ide-plugin/)
Dans IDE vs-code, rien à ajouter, on clique sur le bouton Upload File System et hop (bis) !

Ces fichiers sont permanents, il n'est donc pas nécessaire de les recharger à chaque mise à jour, même si ça peut être une précaution utile ;-)

## éléments clés dans la voiture

* 4 capteurs de choc qui permettent de détecter une collision, ils sont placés dans la portière avant, la portière arrière, dans le bandeau latéral et sur le devant de la voiture.
* 1 détecteur de passager placé dans le siège passager
* 2 Prétensionneur placés dans le montant qui tiens les ceinture conducteur et passager
* 1 airbag rideau, placé au dessus de la vitre de la portière avant côté passager
* 1 airbag latéral côté passager
* 2 airbag frontaux (conducteur et passager)
* 1 batterie
* 1 ECU (Engine Control Unit) qui en fonction des informations récus des capteurs déclenche les actionneurs

## scénarios fonctionnels

Au démarrage du système le chemin de LED entre la batterie et l'ECU est allumé, il faut une action sur la tablette pour le désactiver.
2 Modes sont alors possibles : 
* pas à pas : un bouton sur la tablette doit permettre de passer l'étape suivante du scénario
* continu : le scénario se joue sans interuption

Un bouton permet de simuler la présence ou non d'un passager, si un passager est présent, le ruban entre le siège et l'ECU est allumé. Il est éteint sinon.


Une fois la batterie désactivée, un appui sur les capteurs de choc permet de simuler un choc latéral, on peut également appuyer sur l'avant de la voiture pour simuler un choc frontal.
Si l'on appuie sur le montant latéral, cela déclenche les 3 capteurs de chocs, un choc sur une portière ne déclenche que le capteur lié.
Quand un capteur détecte un choc, le ruban le liant à l'ECU s'allume en chenillard et la LED du capteur s'allume. Si l'ECU recoit une seule information de choc, rien ne se déclenche. S'il y en a au moins 2, il déclenche les actionneurs. Les différents scénarios :
* pour un choc frontal : l'ECU active les prétensionneurs puis les airbags frontaux
* pour un choc latéral : l'ECU actionne les prétensionneurs puis l'airbag latéral

S'il n'y a pas de passager, les airbags et le prétentionneur ne se déclencheront pas côté passager.

Sur les actionneurs (airbags, prétentionneurs) une LED clignote pour indiquer qu'il est activé.

## Positions des rubans
Le diagramme de répartition des rubans. 

![talisman-rubans](./images/Diagramme_talisman.png)


## Déroulé des scénarios

![deroul_secnar](./images/diagramme_fonctionnement_choc.png)

