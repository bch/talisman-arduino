package fr.labobine.talisman;

import android.os.AsyncTask;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class HttpCallTask extends AsyncTask<String, Void, Integer> {
    private Exception exception = null;

    protected Integer doInBackground() {
        return doInBackground();
    }

    protected Integer doInBackground(String... urls) {
        try {
            OkHttpClient client = new OkHttpClient();

            HttpUrl.Builder urlBuilder = HttpUrl.parse(urls[0]).newBuilder();
            urlBuilder.addQueryParameter("origin", "talisman-android");

            String url = urlBuilder.build().toString();

            Request request = new Request.Builder().url(url).build();
            Response response = client.newCall(request).execute();
            return response.code();
        } catch (Exception e) {
            this.exception = e;
        }
        return -1;
    }

    public Exception getException() {
        return exception;
    }
}