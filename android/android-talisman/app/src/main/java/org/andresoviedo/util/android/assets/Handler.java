package org.andresoviedo.util.android.assets;

import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

import org.andresoviedo.util.android.AndroidURLConnection;

/**
 *  App's assets URL handler
 */
public class Handler extends URLStreamHandler {

    @Override
    protected URLConnection openConnection(final URL url) {
        return new AndroidURLConnection(url);
    }

}