#ifndef RUBANS_H
#define RUBANS_H

#include <Arduino.h>
#include <FastLED.h>
#include "headers.h"

CRGB *listLeds[STRIP_COUNT]; // Pointeur vers les buffer des rubans

CRGB *listMotifs[STRIP_COUNT]; // Pointeur vers la liste des motifs

CRGB *eraser = nullptr;

enum StripState : uint8_t // L'état de l'animation arrêtée ou en marche
{
  stopped = 0,
  running = 1
};

enum Commande : uint8_t // commande envoyée au ruban concerné
{
  Stop = 0,
  Start = 1,
  Resume = 2
};

enum Sens : bool // Sens de l'animation normal ou inverse
{
  UP = true,
  DOWN = false
};

/** \struct strip
 * \brief Une structure permettant de manipuler les rubans de leds
 */
struct strip
{
  CRGB *strip;                    /**< Pointeur vers le buffer du ruban */
  uint8_t nb_leds;                /**< Nombre de leds du ruban */
  uint8_t firstLed = 0;           /**< Première led de la partie chenillard du ruban */
  uint8_t lastLed = 30;           /**< Dernière led de la partie chenillard du ruban */
  int8_t nb_boucles = -1;         /**< Nombre de boucles à parcourir -1 pas de limite */
  int8_t currentBoucle = -1;      /**< Numéro de la boucle en cours */
  uint8_t currentState = stopped; /**<  État du ruban (running ou stopped) */
  uint8_t currentStep = 0;        /**< N° de la première led allumée */
  bool stripSens = UP;
};

uint8_t m_currentStrip = 0;   // Le ruban courant
strip listStrip[STRIP_COUNT]; // Liste des rubans

/** \brief Initialise les Rubans de leds avec les valeurs données dans settings.h
 * 
 * - Crée le buffer de leds 
 * - Calcule le motif des leds allumées lors de l'animation, la dernière étant noire
 **/
void initStrips()
{
  // Création du buffer de leds : longueur = nb de leds + 2 fois (la longueur du motif-1)
  for (uint8_t i = 0; i < STRIP_COUNT; i++)
  {
    listLeds[i] = new CRGB[Tab_NB_LEDS[0] + 2 * LIGHTNING_LEDS_COUNT - 2];
  }
  // Création paramètrage des rubans avec les valeurs définies dans settings.h
  for (uint8_t i = 0; i < STRIP_COUNT; i++)
  {
    listStrip[i].strip = listLeds[i];
    listStrip[i].nb_boucles = Tab_Boucles[i];
    listStrip[i].nb_leds = Tab_NB_LEDS[i];
    listStrip[i].currentBoucle = Tab_Boucles[i];
    listStrip[i].stripSens = Tab_StripSens[i];
    listStrip[i].firstLed = Tab_First_Last_Leds[i][0];
    listStrip[i].lastLed = Tab_First_Last_Leds[i][1];
    if(listStrip[i].stripSens == UP)
    {
      listStrip[i].currentStep = listStrip[i].firstLed;
    }
    else // DOWN
    {
      listStrip[i].currentStep = listStrip[i].lastLed;
    }
    
  }

  // Remplissage du tableau des leds allumées (motif[n]) la première étant Black
  for (uint8_t i = 0; i < STRIP_COUNT; i++)
  {
    listMotifs[i] = new CRGB[LIGHTNING_LEDS_COUNT];
    listMotifs[i][0] = CRGB::Black;
    for (uint8_t j = 7; j > 0; j--)
    {
      CHSV color = Tab_COLORS[i];
      color.value = 255 - (j - 1) * 40; // Calcul à revoir (ou pas);
      listMotifs[i][8 - j] = color;
    }
    // Il faut inverser l'ordre des leds du motif pour que le ruban défile dans l'autre sens
    if (listStrip[i].stripSens == DOWN)
    {
      for (int k = 0, j = LIGHTNING_LEDS_COUNT - 1; k < j; k++, j--) // Syntaxe inusitée, mais paratique !
      {
        CRGB temp = listMotifs[i][k];
        listMotifs[i][k] = listMotifs[i][j];
        listMotifs[i][j] = temp;
      }
    }

    // Création de la gomme
      eraser = new CRGB[LIGHTNING_LEDS_COUNT];
    for (uint8_t i = 0; i < LIGHTNING_LEDS_COUNT; i++)
    {
      eraser[i] = CRGB::Black;
    }
  }
}

/** \brief Création des Rubans de leds.
 * 
 * Nécessaire pour l'utilisation de FastLED
 */
void createFastLedsStrips()
{
  // Une limitation de C++ liée aux templates empêche d'utiliser une variable pour le N° du pin
  // offset de LIGHTNINNG_LEDS_COUNT-1 leds comme cela la première led est affichée et les sept autres masquées
  // Les numéros des broches sont définis dans setings.h
  if (STRIP_COUNT > 0)
  {
    FastLED.addLeds<NEOPIXEL, RUBAN_01_PIN>(listLeds[0] + LIGHTNING_LEDS_COUNT - 1, Tab_NB_LEDS[0]);
  }
  if (STRIP_COUNT > 1)
  {
    FastLED.addLeds<NEOPIXEL, RUBAN_02_PIN>(listLeds[1] + LIGHTNING_LEDS_COUNT - 1, Tab_NB_LEDS[1]);
  }
  if (STRIP_COUNT > 2)
  {
    FastLED.addLeds<NEOPIXEL, RUBAN_03_PIN>(listLeds[2] + LIGHTNING_LEDS_COUNT - 1, Tab_NB_LEDS[2]);
  }
  if (STRIP_COUNT > 3)
  {
    FastLED.addLeds<NEOPIXEL, RUBAN_04_PIN>(listLeds[3] + LIGHTNING_LEDS_COUNT - 1, Tab_NB_LEDS[3]);
  }
  if (STRIP_COUNT > 4)
  {
    FastLED.addLeds<NEOPIXEL, RUBAN_05_PIN>(listLeds[4] + LIGHTNING_LEDS_COUNT - 1, Tab_NB_LEDS[4]);
  }
  if (STRIP_COUNT > 5)
  {
    FastLED.addLeds<NEOPIXEL, RUBAN_06_PIN>(listLeds[5] + LIGHTNING_LEDS_COUNT - 1, Tab_NB_LEDS[5]);
  }
  if (STRIP_COUNT > 6)
  {
    FastLED.addLeds<NEOPIXEL, RUBAN_07_PIN>(listLeds[6] + LIGHTNING_LEDS_COUNT - 1, Tab_NB_LEDS[6]);
  }
  if (STRIP_COUNT > 7)
  {
    FastLED.addLeds<NEOPIXEL, RUBAN_08_PIN>(listLeds[7] + LIGHTNING_LEDS_COUNT - 1, Tab_NB_LEDS[7]);
  }
  if (STRIP_COUNT > 8)
  {
    FastLED.addLeds<NEOPIXEL, RUBAN_09_PIN>(listLeds[8] + LIGHTNING_LEDS_COUNT - 1, Tab_NB_LEDS[8]);
  }
  if (STRIP_COUNT > 9)
  {
    FastLED.addLeds<NEOPIXEL, RUBAN_10_PIN>(listLeds[9] + LIGHTNING_LEDS_COUNT - 1, Tab_NB_LEDS[9]);
  }
}

/** \fn writeToSerial(uint8_t numStrip)
 * \brief Ecris sur le port m_serial un code de la forme <numStrip:numAction>
 * 
 * \param numStrip numéro du ruban concerné
 */
void writeToSerial(uint8_t numStrip)
{
  m_serial.write("<");
  m_serial.print(numStrip);
  m_serial.write(":");
  m_serial.print(listStrip[numStrip].currentState);
  m_serial.println(">");
}

/** \fn nextStep(uint8_t numRuban)
 * \brief Gère l'avancement de l'animation du ruban passé en paramètre
 * 
 * \param numRuban numéro du ruban concerné
 */
void nextStep(uint8_t numRuban)
{
  if (listStrip[numRuban].currentState == stopped)
  {
    return; // On retourne immédiatement car il n'y a rien à faire
  }

  // L'animation se déroule dans le sens normal
  if (listStrip[numRuban].stripSens == UP)
  {
    // Si on est dans la dernière boucle et à la fin de l'animation, alors on arrête
    if (listStrip[numRuban].currentBoucle == 0 && listStrip[numRuban].currentStep >= listStrip[numRuban].lastLed )
    {
      // On copie brutalement le motif qui compte 24 octets (8 pixels*3 octets par couleur)
      // quant à listLeds[numRuban] + listStrip[numRuban].currentStep revoir les cours sur l'arithmétique des pointeurs
      // memcpy(listLeds[numRuban] + listStrip[numRuban].currentStep, eraser, LIGHTNING_LEDS_COUNT * 3);
      FastLED.show(); // Afin de prendre en compte l'extinction des dernières leds
      listStrip[numRuban].currentState = stopped;

      writeToSerial(numRuban); // On informe l'ESP WebServer que l'animation s'est terminée
      return;                  // On est arrivé au bout de l'animation
    }

    // Si on est à la fin du ruban, mais pas dans la dernière boucle, on décrémente le nombre de boucles restantes
    if (listStrip[numRuban].currentStep >= listStrip[numRuban].lastLed )
    {
      // On copie brutalement le motif qui compte 24 octets (8 pixels*3 octets par couleur)
      // quant à listLeds[numRuban] + listStrip[numRuban].currentStep revoir les cours sur l'arithmétique des pointeurs
      // memcpy(listLeds[numRuban] + listStrip[numRuban].currentStep, eraser, LIGHTNING_LEDS_COUNT * 3);
      // FastLED.show();
      listStrip[numRuban].currentStep = 0; // On recommence au début
      listStrip[numRuban].currentBoucle--; // boucle suivante
    }
  }
  // L'animation se déroule dans le sens inverse
  else
  {
    // Si on est dans la dernière boucle et à la fin de l'animation, alors on arrête
    if (listStrip[numRuban].currentBoucle == 0 && listStrip[numRuban].currentStep == listStrip[numRuban].firstLed + LIGHTNING_LEDS_COUNT - 1) // 255 car on est sur des uint8_t
    {
      FastLED.show();
      listStrip[numRuban].currentState = stopped;
      writeToSerial(numRuban); // On informe l'ESP WebServer que l'animation s'est terminée
      return;                  // On retourne immédiatement car il n'y a rien à faire
    }
    // Si on est à la fin du ruban, mais pas dans la dernière boucle, on décrémente le nombre de boucles restantes
    if (listStrip[numRuban].currentStep == listStrip[numRuban].firstLed + LIGHTNING_LEDS_COUNT -1)
    {
      listStrip[numRuban].currentStep = listStrip[numRuban].lastLed; // On recommence à la fin du ruban
      listStrip[numRuban].currentBoucle--;                                                      // boucle suivante
    }
  }
  // On copie brutalement le motif qui compte 24 octets (8 pixels*3 octets par couleur)
  // quant à listLeds[numRuban] + listStrip[numRuban].currentStep revoir les cours sur l'arithmétique des pointeurs
  memcpy(listLeds[numRuban] + listStrip[numRuban].currentStep, listMotifs[numRuban], LIGHTNING_LEDS_COUNT * 3);
  FastLED.show();
  memcpy(listLeds[numRuban] + listStrip[numRuban].currentStep, eraser, LIGHTNING_LEDS_COUNT * 3); // On éteint les leds, ce sera fait au prochain show()

  // On passe à l'étape suivante
  if (listStrip[numRuban].stripSens == UP)
  {
    listStrip[numRuban].currentStep++; // On passe à l'étape suivante
  }
  else
  {
    listStrip[numRuban].currentStep--; // On passe à l'étape suivante : on dans le sens inverse
  }
}

/** \fn clearStrip(uint8_t numRuban)
 * \brief Éteint toutes les leds du ruban passé en paramètre
 * 
 * \param numRuban le numéro du ruban concerné
 */
void clearStrip(uint8_t numRuban)
{
  // On remplit directement le buffer du ruban avec la valeur 0 (noir) et on compte 3 octets par led plus les octets de début et fin
  memset(listStrip[numRuban].strip, 0, 3 * (listStrip[numRuban].nb_leds + 2 * (LIGHTNING_LEDS_COUNT - 1))); // Remise à zéro du ruban concerné
}

/** \fn startStrip(uint8_t numRuban)
 * \brief Démarre l'animation du ruban passé en paramètre
 * 
 * \param numRuban le numéro du ruban concerné
 */
void startStrip(uint8_t numRuban)
{
  // On remet le ruban au début et on éteint toutes les leds
  listStrip[numRuban].currentBoucle = listStrip[numRuban].nb_boucles - 1;
  if (listStrip[numRuban].stripSens == UP)
  {
    listStrip[numRuban].currentStep = listStrip[numRuban].firstLed ; // premiere case du buffer
  }
  else // DOWN
  {
    listStrip[numRuban].currentStep = listStrip[numRuban].lastLed ; // dernière case dans laquelle on a le droit d'écrire
  }
  listStrip[numRuban].currentState = running;
  clearStrip(numRuban);
  // writeToSerial(numRuban);
}

/** \fn stopStrip(uint8_t numRuban)
 * \brief Stop le déroulement de l'animation du ruban passé en paramètre
 * 
 * \param numRuban le numéro du ruban concerné
 */
void stopStrip(uint8_t numRuban)
{
  listStrip[numRuban].currentState = stopped;
  // writeToSerial(numRuban);
}

/** \fn resumeStrip(uint8_t numRuban)
 * \brief Reprend l'animation du ruban passé en paramètre
 * 
 * \param numRuban le numéro du ruban concerné
 */
void resumeStrip(uint8_t numRuban)
{
  listStrip[numRuban].currentState = running;
  // writeToSerial(numRuban);
}

/** \fn clearAllStrips()
 * \brief Éteint toutes les leds de tous les rubans
 * 
 */
void clearAllStrips()
{
  for (uint8_t i = 0; i < STRIP_COUNT; i++)
  {
    stopStrip(i);
    clearStrip(i);
    FastLED.show();
  }
}

#endif //RUBANS_H