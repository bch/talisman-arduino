#ifndef COM_H
#define COM_H

#include <Arduino.h>
#include <WiFi.h>

#include "settings.h"

#define OUTPUT_SERIAL_PRINT 1

/** \fn startWifi()
 * \brief Démarre le wifi avec les paramètres fournis dans settings.h
 */
void startWifi()
{
      // Configures static IP address
  if (!WiFi.config(local, gateway, subnet))
   {
    Serial.println("Échec de la configuration STA");
  }
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
#if OUTPUT_SERIAL_PRINT
        Serial.print(".");
#endif
    }
#if OUTPUT_SERIAL_PRINT
    Serial.println("");
    Serial.println("WiFi connecté.");
    Serial.println("IP addresse: ");
    Serial.println(WiFi.localIP());
#endif
}

/** \fn stopWifi()
 * \brief Arrête le wifi
 */
void stopWifi()
{
    WiFi.mode(WIFI_OFF);
    while (WiFi.status() == WL_CONNECTED)
    {
        delay(500);
#if OUTPUT_SERIAL_PRINT
        Serial.print(".");
#endif
    }
#if OUTPUT_SERIAL_PRINT
    Serial.println("WiFi déconnecté.");
#endif
}

#endif // COM_H
