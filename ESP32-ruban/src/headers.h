#ifndef HEADERS_RUBAN_H
#define HEADERS_RUBAN_H
#include <Arduino.h>

#include "settings.h"

// FIchier de déclaration des fonctions de main.cpp

void writeToSerial(uint8_t numStrip);
void startWifi();
void stopWifi();
void commandeStrip(uint8_t numStrip, uint8_t action);


#endif