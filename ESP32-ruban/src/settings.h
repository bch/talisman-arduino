#ifndef SETTINGS_H
#define SETTINGS_H

#include <Arduino.h>
#include <FastLED.h>
///////////////////   Le Wifi       ///////////////////////////////
const char *ssid = "talisman";      // Identifiant du réseau
const char *password = "123456789"; // Mot de passe réseau
IPAddress local(192, 168, 4, 10);   // L'adresse du microcontroleur des rubans
IPAddress gateway(192, 168, 4, 1);  // Adresse du l'autre ESP
IPAddress subnet(255, 255, 255, 0);

/////////////////// Liaison série    //////////////////////////////
const uint8_t RXPIN = 16;   // Broche RX pour le port série avec ESP32
const uint8_t TXPIN = 17;   // Broche TX pour le port série avec ESP32
HardwareSerial m_serial(1); // Port série ne pas modifier

//////////////////// Les LEDs        ///////////////////////////////
const uint8_t STRIP_COUNT = 2;                  // Le nombre de rubans
const uint8_t Tab_NB_LEDS[STRIP_COUNT] = {60,
                                          30};  // Le nombre de leds de chaque ruban
const uint LIGHTNING_LEDS_COUNT = 8;            // La dernière led est "noire", donc 7 leds allumées
const CHSV Tab_COLORS[STRIP_COUNT] = {CHSV(224, 255, 255),
                                      CHSV(0, 255, 255)}; // Les couleurs des motifs
                                      
int8_t Tab_Boucles[STRIP_COUNT] = {3,
                                   2};      // -1 le nombre de cycles est infini, sinon, compris entre 0 et 127

bool Tab_StripSens[STRIP_COUNT] = {false,
                                    false}; // true dans le sens montant, false dans l'autre
uint8_t Tab_First_Last_Leds[STRIP_COUNT][2] = {{9, 50},
                                               {0, 30}};

// Les broches auxquelles sont rattachés les rubans

#define RUBAN_01_PIN 25
#define RUBAN_02_PIN 26
#define RUBAN_03_PIN 32
#define RUBAN_04_PIN 33
#define RUBAN_05_PIN 18
#define RUBAN_06_PIN 19
#define RUBAN_07_PIN 21
#define RUBAN_08_PIN 22
#define RUBAN_09_PIN 23
#define RUBAN_10_PIN 27


uint16_t m_stepDuration = 25; // ms : règle la vitesse d'affichage des Leds

#define SERVER_PORT 8888
#endif // SETTINGS_H