/* Projet Talisman
  * Une petite appli pour gérer un (des) ruban(s) de leds
  *
  * @author 2020 Jean-Louis Frucot <frucot.jeanlouis@free.fr>
  * @see The GNU Public License (GPL)
  *
  * this program is free software you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY
  * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  * for more details.
  *
  * You should have received a copy of the GNU General Public License along
  * with this program. If not, see <http://www.gnu.org/licenses/>.
  */

#include <Arduino.h>
#include <FastLED.h>

#include <WiFi.h>
#include <ESPAsyncWebServer.h>
#include <AsyncElegantOTA.h>

////////////////////////////// Mes inclusions   /////////////////////////////////////////////
#include "settings.h"
#include "rubans.h"
#include "com.h"
#include "JLF_Timer.h"

#define OUTPUT_SERIAL_PRINT 1 // 1 affiche les debugs, 0 ne les affiche pas

///////////////////////////// Initialisations des variables  /////////////////////////////////
char octetReception = 0;      // variable de stockage des valeurs reçues sur le port Série (ASCII)
String chaineReception = "";  // déclare un objet String vide pour reception chaine

AsyncWebServer server(SERVER_PORT); // Serveur sur le port defini dans settings.h
// Création d'un timer pour rythmer l'affichage des leds
JLF_Timer *m_stripTimer = new JLF_Timer(m_stepDuration); // ms

void setup()
{
#if OUTPUT_SERIAL_PRINT
  Serial.begin(9600);
  delay(5000); // On attend que le serveur du deuxième ESP32 soit démarré
  Serial.print("Connexion à ");
  Serial.println(ssid);
#endif
  startWifi(); // Connect to Wi-Fi network with SSID and password

  // initialisation des rubans
  initStrips();
  createFastLedsStrips();

  // Création de la liaison série entre les deux microcontroleurs
  m_serial.begin(9600, SERIAL_8N1, RXPIN, TXPIN);
  delay(200); // On laisse un peu de temps pour que les choses se mettent en place

  // Route pour " /" web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(200, "text/plain", "Hello, Vous êtes sur le serveur ESP32-Rubans");
  });
  AsyncElegantOTA.begin(&server); // Start ElegantOTA à l'adresse /update/
  server.begin();

  // C'est parti !
  m_stripTimer->start();
  FastLED.clear(true);
  FastLED.show();

  stopWifi(); // On arrête le wifi pour des économies d'énergie
}

void loop()
{
  if (WiFi.status() == WL_CONNECTED)
  {
    AsyncElegantOTA.loop(); // On guette une demande de mise à jour du firmware
  }
  ////////////////////  Gestion du port série    ////////////////////////////
  while (m_serial.available() >= 1)
  {
    octetReception = m_serial.read(); // Lit le 1er octet reçu et le met dans la variable
    if (octetReception == char('<'))
    {
      chaineReception = m_serial.readStringUntil('>'); // Le '>' n'est pas compris dans la chaine de retour
      Serial.println(chaineReception);
    }

    int8_t indexSeparateur = chaineReception.indexOf(":");
    if (indexSeparateur == -1)
    {
      break;
    }                                                                            // On repère la place du ":"
    uint8_t m_ruban = chaineReception.substring(0, indexSeparateur).toInt();     // On extrait le numéro du ruban
    uint8_t m_commande = chaineReception.substring(indexSeparateur + 1).toInt(); // On extrait le numéro de la commande
    commandeStrip(m_ruban, m_commande);                                          // Lancer la commande

#if OUTPUT_SERIAL_PRINT
    Serial.print("Ruban N°");
    Serial.print(m_ruban);
    Serial.print(" Commande N°");
    Serial.println(m_commande);
#endif
    chaineReception = ""; // Réinitialisation de la chaine de réception
    break;                // sort de la boucle while
  }

  // ici, on gère l'affichage à chaque fois que le timer a basculé
  if (m_stripTimer->isTimeElapsed())
  {
    for (uint8_t i = 0; i < STRIP_COUNT; i++)
    {
      if (listStrip[i].currentState == running)
      {
        nextStep(i); // On met à jour le ruban si nécessaire
      }
    }
  }
}

void commandeStrip(uint8_t numStrip, uint8_t action)
{
  if (numStrip < STRIP_COUNT)
  {
    switch (action)
    {
      // On lance l'action correspondant au code reçu
    case Stop:
      stopStrip(numStrip);
      clearStrip(numStrip);
      FastLED.show();
#if OUTPUT_SERIAL_PRINT
      Serial.println("ClearStrip : " + String(numStrip));
#endif
      break;
    case Start:
      startStrip(numStrip);
#if OUTPUT_SERIAL_PRINT
      Serial.println("startStrip : " + String(numStrip));
#endif
      break;
    case Resume:
      resumeStrip(numStrip);
#if OUTPUT_SERIAL_PRINT
      Serial.println("resumeStrip : " + String(numStrip));
#endif
      break;
    default:
      // On ne fait rien
      break;
    }
  }
  else
  {
    switch (numStrip)
    {
    case 99:
      clearAllStrips();
      break;
    case 98:
      if (action > 0)
      {
        startWifi();
      }
      else
      {
        stopWifi();
      }

      break;
    default:
      break;
    }
  }
}