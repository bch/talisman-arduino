/* Projet Talisman
  * Une petite appli pour gérer un (des) ruban(s) de leds
  *
  * @author 2020 Jean-Louis Frucot <frucot.jeanlouis@free.fr>
  * @see The GNU Public License (GPL)
  *
  * this program is free software you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY
  * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  * for more details.
  *
  * You should have received a copy of the GNU General Public License along
  * with this program. If not, see <http://www.gnu.org/licenses/>.
  */

#include <Arduino.h>
#include <Adafruit_NeoPixel.h>
#include <SoftwareSerial.h>

#include "JLF_Timer.h"
#include "ruban.h"
#define OUTPUT_SERIAL_PRINT 0 // 1 affiche les debugs, 0 ne les affiche pas

////////////////////////////////////////////// Déclaration des variables

const uint8_t NB_RUBANS = 2;                        // Les tableaux ci-dessous doivent avoir le même nombre d'éléments que NB_RUBAN
const uint8_t PINS[NB_RUBANS] = {6, 7};                // N° des broches auxquelles sont connectés les rubans
const uint8_t LEDSCOUNTS[NB_RUBANS] = {60, 120};         // NOmbre de leds sur chaque ruban (dans le même ordre que PINS[]) limité à 255 LEDs
const uint8_t COLORS[NB_RUBANS][3] = {{255, 0, 0}, {0,255,0}}; // Tableau des couleurs de chaque ruban (dans le même ordre que PINS[])
const uint8_t TAILLEMOTIF = 7;
unsigned char MOTIFS[NB_RUBANS][TAILLEMOTIF] = {{1, 4, 8, 16, 32, 64, 128},{1, 4, 8, 16, 32, 64, 128}}; // diviseur de luminosité pour les leds allumées
const int8_t BOUCLES[NB_RUBANS] = {2, 2};
const neoPixelType TYPES[NB_RUBANS] = {NEO_RGB + NEO_KHZ800};

const uint8_t RXPIN = 4; // Broche RX pour le port série avec ESP32
const uint8_t TXPIN = 5; // Broche TX pour le port série avec ESP32
////////////////////////////////////////////// Déclaration des fonctions et structures et autres enum//

enum RubanState : uint8_t
{
  stopped = 0,
  running = 1
};

struct strip
{
  Ruban *ptr_Ruban;
  int8_t nb_boucles;
  uint8_t nb_leds;
  int8_t currentBoucle = -1;
  uint8_t currentState = running;
  uint8_t currentStep = 0;
};

void createRubans();

strip listRubans[NB_RUBANS];
uint16_t m_stepDuration = 25; //ms règle la vitesse d'affichage des Leds

// Création d'un timer pour rythmer l'affichage des leds
JLF_Timer *m_stripTimer = new JLF_Timer(m_stepDuration); // ms

// Ouverture d'un port série pour communiquer avec ESP32 serveur

SoftwareSerial m_serial(RXPIN, TXPIN);

void setup()
{
  Serial.begin(9600);

  createRubans();
  for (int8_t i = 0; i < NB_RUBANS; i++)
  {
    listRubans[i].ptr_Ruban->clear();
    listRubans[i].ptr_Ruban->show();
  }

  m_serial.begin(9600);
  // delay(2000);
  m_stripTimer->start();
  listRubans[0].ptr_Ruban->restart(); // Juste pour un test
  listRubans[1].ptr_Ruban->restart(); // Juste pour un test
}

void loop()
{

  if (m_serial.available())
  {
    Serial.print("available ");
    uint8_t message = 0;
    message = m_serial.read(); // le caractère reçu doit être entre 60 et 60+NB_RUBANS-1 ou 100 et 100+NB_RUBANS-1
    Serial.println(message);
    if (message >= 100 && message <= 100 + NB_RUBANS - 1)
    {
      listRubans[message - 100].ptr_Ruban->stop();
    }
    else
    {
      if (message >= 60 && message <= 60 + NB_RUBANS - 1)
      {
        listRubans[message - 60].ptr_Ruban->restart();
      }
    }
  }
  // ici, on gère l'affichage à chaque fois que le timer a basculé
  if (m_stripTimer->isTimeElapsed())
  {
    for (uint8_t i = 0; i < NB_RUBANS; i++)
    {
      if (listRubans[i].ptr_Ruban->isStopped() == false)
      {
#if OUTPUT_SERIAL_PRINT
        Serial.print("Pointeur ");
        Serial.print("Stopped : ");
        Serial.println(listRubans[i].ptr_Ruban->isStopped());
#endif
        // On regarde s'il faut avancer d'un pas ou si on est arrivé au bout
        if (listRubans[i].nb_boucles == -1)
        {
#if OUTPUT_SERIAL_PRINT
          Serial.println("Boucle infinie");
#endif
          // On continue de boucler indéfiniment
          listRubans[i].currentStep = listRubans[i].ptr_Ruban->nextStep();
        }
        else
        {
// On avance d'un pas et on vérifie s'il reste des pas à faire;
#if OUTPUT_SERIAL_PRINT
          Serial.print("Nombre de boucle restant : ");
          Serial.println(listRubans[i].currentBoucle);
#endif
          if (listRubans[i].currentBoucle > 0) // Il reste des boucles à faire
          {
#if OUTPUT_SERIAL_PRINT
            Serial.println("Boucle sup 0");
#endif
            // On continue de boucler
            listRubans[i].currentStep = listRubans[i].ptr_Ruban->nextStep();
            if (listRubans[i].currentStep == listRubans[i].nb_leds + 7 - 1) // Est-on arrivé au bout de ruban ?
            {
              listRubans[i].currentBoucle--;
            }
          }
          else
          {
            // On termine la dernière boucle
            if (listRubans[i].currentStep == listRubans[i].nb_leds)
            {
#if OUTPUT_SERIAL_PRINT
              Serial.println("Dernière boucle");
#endif
              // On a fini
              listRubans[i].ptr_Ruban->stop();
            }
            else
            {
              listRubans[i].ptr_Ruban->nextStep();
            }
          }
        }
      }
    }
  }
}
void createRubans()
{
  // On crée le nombre de rubans déclaré dans NB_RUBANS
  for (uint8_t i = 0; i < NB_RUBANS; i++)
  {
    listRubans[i].ptr_Ruban = new Ruban(LEDSCOUNTS[i], PINS[i], TYPES[i]);
    listRubans[i].ptr_Ruban->begin();
    listRubans[i].ptr_Ruban->setColor(COLORS[i][0], COLORS[i][1], COLORS[i][2]);
    listRubans[i].ptr_Ruban->setMotif(MOTIFS[i], TAILLEMOTIF);
    listRubans[i].nb_leds = LEDSCOUNTS[i];
    listRubans[i].nb_boucles = BOUCLES[i];
    listRubans[i].currentBoucle = BOUCLES[i];
    listRubans[i].ptr_Ruban->clear();
    listRubans[i].ptr_Ruban->show();
#if OUTPUT_SERIAL_PRINT
    Serial.print("Ruban N°");
    Serial.println(i);
#endif
  }
}
////////////////////////////////// Définition des fonctions //////////
