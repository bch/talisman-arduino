#ifndef SETTINGS_H
#define SETTINGS_H

#include <Arduino.h>

const uint8_t STRIP_COUNT = 10;         // Nombre de rubans

// Identifiants du wifi
const char *ssid = "talisman";          // Identifiant du réseau
const char *password = "123456789";     // Mot de passe du réseau

// Adresses pour le wifi
IPAddress local(192, 168, 4, 1);        // Adresse IP du serveur
IPAddress gateway(192, 168, 4, 1);
IPAddress subnet(255, 255, 255, 0);

#define SERVER_PORT 8888

///////////////  Port Serie   ///////////////////////////////////
const uint8_t RXPIN = 16; // Broche RX pour le port série avec ESP32
const uint8_t TXPIN = 17; // Broche TX pour le port série avec ESP32

#endif