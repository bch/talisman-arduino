/* Projet Talisman
  * Une petite appli pour gérer la communication avec la tablette et les rubans de leds
  *
  * @author 2020 Jean-Louis Frucot <frucot.jeanlouis@free.fr>
  * @see The GNU Public License (GPL)
  *
  * this program is free software you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY
  * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  * for more details.
  *
  * You should have received a copy of the GNU General Public License along
  * with this program. If not, see <http://www.gnu.org/licenses/>.
  */

#include "headers.h"
#include "server.h" // Initialise les callback en fonction de l'URL reçue

#define OUTPUT_SERIAL_PRINT 1 // 1 affiche les debugs, 0 ne les affiche pas

AsyncWebServer server(SERVER_PORT); // On déclare un serveur sur le port defini dans settings.h

HardwareSerial m_serial(1);

void setup()
{
  ////////////////////////    Initialisation des ports série   //////////////////////////////////
  Serial.begin(9600);
  m_serial.begin(9600, SERIAL_8N1, RXPIN, TXPIN);
  delay(200); // On laisse un peu de temps pour que les choses se mettent en place

  ////////////////////////    Paramètrage du serveur en tant que Access Point ///////////////////
  bool ok = WiFi.softAP(ssid, password, 1); // On fixe les IP
#if OUTPUT_SERIAL_PRINT
  Serial.print("Setting soft-AP ... ");
  Serial.println(ok ? "OK" : "Échoué!"); // Création du Point d'accès
#endif
  delay(2000);                               // Pour que tout se passe bien
  WiFi.softAPConfig(local, gateway, subnet); // On fixe les IP
#if OUTPUT_SERIAL_PRINT
  Serial.print("Soft-AP IP address = ");
  Serial.println(WiFi.softAPIP());
#endif

  /////////////////////// Initialize SPIFFS pour stoker les fichiers html, css, js... //////////
  if (!SPIFFS.begin(true))
  {
#if OUTPUT_SERIAL_PRINT
    Serial.println("Une erreur et survenue lors du montage SPIFFS");
#endif
    return;
  }

  initServer(server); // On initialise les callback en fonction des URL reçues

  AsyncElegantOTA.begin(&server); // Démarre ElegantOTA à l'adresse /update/

  server.begin();

  clearAllRubans(); // On demande la mise à zéro des rubans
}

void loop()
{
  AsyncElegantOTA.loop();

  while (m_serial.available())
  {
    /* On reçoit des bytes issus de l'ESP-Ruban normalement à chaque modif d'un statut de ruban.
       On met alors à jour le tableau de statut des rubans, même si ça à déjà été fait dans toggleStrip
    */
    m_serial.readStringUntil('<');                  // le signe '<' n'est pas compris dans la chaine.
    String message = m_serial.readStringUntil('>'); // le signe '>' n'est pas compris dans la chaine.
    // updateStripStatus(message);                     // On obtient un message de la forme numRuban:numAction (ie 0:1)
  }
}

void toggleStrip(uint8_t numRuban)
{
  tab_StripStatus[numRuban] = (tab_StripStatus[numRuban] == ON) ? OFF : ON;
  // On va écrire sur le port série vers l'ESP32 des rubans
  String message = String("<" + String(numRuban) + ":" + String(tab_StripStatus[numRuban]) + ">");
  m_serial.println(message);
#if OUTPUT_SERIAL_PRINT
  Serial.println(message);
#endif
}

void clearAllRubans()
{
  String message = String("<" + String(99) + ":" + String(0) + ">");
  m_serial.println(message);
#if OUTPUT_SERIAL_PRINT
  Serial.println(message);
#endif
  for (uint8_t i = 0; i < STRIP_COUNT; i++) // On met le tableau des statuts à jour
  {
    tab_StripStatus[i] = OFF;
  }
}
// void notFound(AsyncWebServerRequest *request)
// {
//   request->send(404, "text/plain", "Page non trouvée");
// #if OUTPUT_SERIAL_PRINT
//   Serial.println("Page non trouvée");
// #endif
// }

void updateStripStatus(String message)
{
  // Si le message est de la bonne forme, on met à jour le tableau des status des rubans
  uint8_t indexSeparateur = message.indexOf(':');
  if (indexSeparateur != -1) // Si pas de : alors le message n'est pas bon
  {
    uint8_t numRuban = message.substring(0, indexSeparateur).toInt();     // On extrait le numéro du ruban
    uint8_t numCommande = message.substring(indexSeparateur + 1).toInt(); // On extrait le numéro de la commande
    if (numRuban < STRIP_COUNT && numCommande < 2)                        // On vérifie que les paramètres sont plausibles
    {
      tab_StripStatus[numRuban] = numCommande;
    }
  }
}

void startWifiRuban()
{
  String message = String("<" + String(98) + ":" + String(1) + ">");
  m_serial.println(message);
#if OUTPUT_SERIAL_PRINT
  Serial.println(message);
#endif
}
