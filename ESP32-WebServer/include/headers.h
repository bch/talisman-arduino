#ifndef HEADERS_H
#define HEADERS_H

#include <Arduino.h>
#include <WiFi.h>
#include <ESPAsyncWebServer.h>
#include <AsyncElegantOTA.h>
#include <FS.h>
#include <SPIFFS.h>


#include "settings.h"

/** Enumération des statuts possibles pour les rubans **/
enum Status : uint8_t
{
  OFF = 0,
  ON = 1
};

/** Tableau des états des rubans  ON/OFF **/
uint8_t tab_StripStatus[STRIP_COUNT] = {OFF};

/** callback gérant une page non trouvée **/
void notFound(AsyncWebServerRequest *request);
/** Demande le démarrage du wifi pour le deuxième ESP **/
void startWifiRuban();
/** Demande l'inversion de l'état du ruban passé en paramètre : running/stopped **/
void toggleStrip(uint8_t numRuban);
/** Demande l'extinction de tous les rubans **/
void clearAllRubans();
/** Met à jour le statut du ruban passé dans message **/
void updateStripStatus(String message);
/** callback permettant de modifier la page html retournée **/
String processor(const String &var);

#endif // HEADER_H