#ifndef SERVER_H
#define SERVER_H

#include "headers.h"
#include "settings.h"

#define OUTPUT_SERIAL_PRINT 1 // 1 affiche les debugs, 0 ne les affiche pas

/** \fn initServer(AsyncWebServer &server)
 * \brief Initialise les callbacks du serveur
 * 
 * \param &server Référence vers le serveur créé dans main.cpp
 */
void initServer(AsyncWebServer &server)
{
  /* Page d'accueil du site talisman */
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(SPIFFS, "/index.html", String(), false);
  });

  /* Si l'adresse contient wifiruban, on demande le démarrage du serveur de l'ESP pilotant les rubans de LEDs */
  server.on("/wifiruban/", HTTP_GET, [](AsyncWebServerRequest *request) {
    startWifiRuban();
    request->send(SPIFFS, "/index.html", String(), false);
  });
  /* Page des boutons de commande des rubans */
  server.on("/command/", HTTP_GET, [](AsyncWebServerRequest *request) {
    if (request->hasParam("ruban"))                          // si on a ce paramètre dans l'url
    {                                                        //
      AsyncWebParameter *param = request->getParam("ruban"); // On récupère la valeur du paramètre ruban
      uint8_t paramValue = param->value().toInt();           // On convertit le paramètre en int
#if OUTPUT_SERIAL_PRINT
      Serial.print("ruban : ");
      Serial.println(paramValue);
#endif
      if (paramValue == 0) // Si la valeur est 0, on éteint tous les rubans
      {
        clearAllRubans();
      }
      else
      {
        if (paramValue <= STRIP_COUNT)
        {
          paramValue--;            // La numérotation sur la tablette commance à 1 au lieu de 0. Si pas content voir Arnaud ;-)
          toggleStrip(paramValue); // Sinon, on change le statut du ruban running/stopped
        }
      }
    }
    bool tablette = false;
    if (request->hasParam("tablette"))
    {
      if (request->getParam("tablette")->value().toInt() == 1)
      {
        tablette = true;
      }
    }
    if (tablette)
    {
      request->send(200, "text/plain", "");
    }
    else
    {
      request->send(SPIFFS, "/talisman.html", String(), false, processor); // Et on retourne la page mise à jour par le "processor"
    }
    
  });

  server.onNotFound(notFound);
}

/** \fn processor(const String &var)
 * \brief Parse le fichier html retourné et remplace les balises par la bonne valeur
 * 
 * \param &var La chaine à modifier
 * \return La chaine modifiée
 */
String processor(const String &var) // Permet de modifier la page de retour talisman.html
{
  uint8_t i = var.substring(7).toInt() - 1; // On extrait le numéro du ruban
  if (var.startsWith("STATUS"))
  {
    return (tab_StripStatus[i] == ON) ? "ON" : "OFF";
  }
  if (var.startsWith("ACTION"))
  {
    return (tab_StripStatus[i] == ON) ? "OFF" : "ON";
  }
  if (var.startsWith("BOUTON"))
  {
    return (tab_StripStatus[i] == ON) ? "button button-off" : "button button-on";
  }
  return String();
}

/** \fn notFound(AsyncWebServerRequest *request)
 * \brief Retourne une page 404 en cas de page non trouvée
 * 
 * \param *request Pointeur vers la requette à l'origine du problème
 */
void notFound(AsyncWebServerRequest *request)
{
  request->send(404, "text/plain", "Page non trouvée");
#if OUTPUT_SERIAL_PRINT
  Serial.println("Page non trouvée");
#endif
}
#endif // SERVER_H